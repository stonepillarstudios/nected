package com.sps.gynoid.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by koell on 28 Jan 2018.
 */

public class PrefsHelper {

    static final String PREF_GUID = "STR_GUID"; //package-private so JanTools has access to it
    public static final String PREF_FIRST_RUN = "BOOL_FIRST_RUN";
    public static final String PREF_LAST_LIKES_UPLOAD = "LONG_LAST_LIKES_UPLOAD";
    public static final String PREF_DIA_REPORT_MSGER = "BOOL_DIALOG_REPORT_BAD_MESSENGER_USERNAME";
    public static final String PREF_REG_DONE = "BOOL_REGISTRATION_DONE";
    public static final String PREF_DIA_LOW_QUALITY = "BOOL_DIA_LOW_QUALITY";
    public static final String PREF_DIA_HIGH_QUALITY = "BOOL_DIA_HIGH_QUALITY";
    public static final String PREF_RAUTH = "STR_RAUTH";
    public static final String PREF_LOCAL_MATCHES = "STR_LOCALLY_SAVED_MATCHES";

    private SharedPreferences prefs;

    public PrefsHelper(Context ctx){
        prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

    }

    public void setDefaults(){

        ContentValues defaults = new ContentValues();
        defaults.put(PREF_FIRST_RUN, true);
        defaults.put(PREF_LAST_LIKES_UPLOAD, 0L);
        defaults.put(PREF_DIA_LOW_QUALITY, false);
        defaults.put(PREF_DIA_HIGH_QUALITY, false);
        defaults.put(PREF_REG_DONE, false);
        defaults.put(PREF_RAUTH, "");
        defaults.put(PREF_LOCAL_MATCHES, "[]");

        for (String key : defaults.keySet()){

            if (!prefs.contains(key)) {
                switch (key.substring(0, key.indexOf("_"))) {
                    case "STR":
                        write(key, defaults.getAsString(key));
                        break;

                    case "BOOL":
                        write(key, defaults.getAsBoolean(key));
                        break;

                    case "INT":
                        write(key, defaults.getAsInteger(key));
                        break;

                    case "LONG":
                        write(key, defaults.getAsLong(key));
                        break;
                }
            }

        }

    }

    public void write(String pref, String value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(pref, value);
        editor.apply();

    }

    public void write(String pref, int value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(pref, value);
        editor.apply();

    }

    public void write(String pref, boolean value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(pref, value);
        editor.apply();
    }

    public void write(String pref, long value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(pref, value);
        editor.apply();
    }

    public String readStr(String pref){
        return prefs.getString(pref, null);

    }

    public int readInt(String pref){
        return  prefs.getInt(pref, -1);

    }

    public boolean readBool(String pref){
        return prefs.getBoolean(pref, false);
    }

    public long readLong(String pref){
        return prefs.getLong(pref, -1L);
    }


}
