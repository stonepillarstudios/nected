package com.sps.gynoid.utils;

import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

//a custom jsonobject that doesn't throw exceptions
public class Rjson extends JSONObject{

    @Override
    public Rjson put(String name, Object value){
        try {
            super.put(name, value);
        }catch (JSONException e){
            Log.w("devlog", "Rjson couldn't set a param: \nname: " + name + "\nvalue: " + value.toString());
        }
        return this;
    }

    @Override
    public Rjson put(String name, int value){
        try {
            super.put(name, value);
        }catch (JSONException e){
            Log.w("devlog", "Rjson couldn't set a param: \nname: " + name + "\nvalue: " + value);
        }
        return this;
    }

    @Override
    public Rjson put(String name, boolean value){
        try {
            super.put(name, value);
        }catch (JSONException e){
            Log.w("devlog", "Rjson couldn't set a param: \nname: " + name + "\nvalue: " + value);
        }
        return this;
    }

    @Override
    public Rjson put(String name, double value){
        try {
            super.put(name, value);
        }catch (JSONException e){
            Log.w("devlog", "Rjson couldn't set a param: \nname: " + name + "\nvalue: " + value);
        }
        return this;
    }

    @Override
    public Rjson put(String name, long value){
        try {
            super.put(name, value);
        }catch (JSONException e){
            Log.w("devlog", "Rjson couldn't set a param: \nname: " + name + "\nvalue: " + value);
        }
        return this;
    }

}
