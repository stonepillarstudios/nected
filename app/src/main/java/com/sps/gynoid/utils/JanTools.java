package com.sps.gynoid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Jans Rautenbach on 2015/09/14.
 */

public class JanTools {

    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    private Context ctx;
    private PrefsHelper prefs;

    public JanTools(Context sCtx){
        ctx = sCtx;
        prefs = new PrefsHelper(ctx);
    }

    public void makeToast(@StringRes final int strID){
        makeToast(strID, true);
    }


    public void makeToast(@StringRes final int strID, boolean forceOutsideFocus){
        makeToast(ctx.getString(strID), forceOutsideFocus);
    }

    public void makeToast(final String s, boolean forceOutsideFocus){
        if (((Activity) ctx).hasWindowFocus() || forceOutsideFocus) {
            ((Activity) ctx).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    public void makeToast(final String s){
        makeToast(s, true);
    }

    static public int indexOfStrArr(String[] array, String search){

        int pos = 0;
        while (!array[pos].equals(search)){
            pos++;
            if (pos >= array.length){
                return -1;
            }
        }
        return pos;

    }

    public String getGUID(){

        String GUID = prefs.readStr(PrefsHelper.PREF_GUID);

        if (GUID == null){
            GUID = UUID.randomUUID().toString();
            prefs.write(PrefsHelper.PREF_GUID, GUID);
        }

        return GUID;

    }

    public static void printArray(Object[] print){
        for (int i = 0; i < print.length; i++){
            Log.d("devlog", ("pos " + i + ": " + print[i]));
        }
    }

    public boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public boolean checkIfWifi(){
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
        }catch (NullPointerException e){
            e.printStackTrace();
            return false;
        }
    }

    public float convertPixelsToDp(float px){
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    public float convertDpToPixels(float dp){
        DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        return dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static void writeLine(String line, BufferedWriter writer) throws IOException {
        writer.write(line);
        writer.newLine();
        writer.flush();
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        return rand.nextInt((max - min) + 1) + min;
    }

    public static String randArr(String[] array){
        return array[randInt(0, array.length-1)];
    }

    public boolean isPackageInstalled(String packagename) {
        PackageManager packageManager = ctx.getPackageManager();
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public ApplicationInfo getAppInfo(String pkg){

        try{
            return ctx.getPackageManager().getApplicationInfo(pkg, 0);
        }catch (PackageManager.NameNotFoundException e){
            Log.d("devlog", "Could not find '"+pkg+"'");
            return null;
        }

    }

    public Drawable getAppIcon(String pkg){

        try{
            return ctx.getPackageManager().getApplicationIcon(pkg);
        }catch (PackageManager.NameNotFoundException e){
            Log.d("devlog", "Could not find '"+pkg+"'");
            return null;
        }

    }

    public void openExternalApp(String pkg){

        ctx.startActivity(ctx.getPackageManager().getLaunchIntentForPackage(pkg));

    }

    public Drawable scaleDrawable(Drawable image, int percent) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, percent, percent, false);
        return new BitmapDrawable(ctx.getResources(), bitmapResized);
    }

    public void hideKeyboard(){
        View view = ((Activity) ctx).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }else{
            Log.d("devlog", "Couldn't hide keyboard");
        }
    }

    public void hideKeyboard(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String getVersionName(){
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }
    public int getVersionCode(){
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode;
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }
}


