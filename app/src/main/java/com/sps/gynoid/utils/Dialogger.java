package com.sps.gynoid.utils;

import android.content.Context;
import android.app.AlertDialog;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.sps.gynoid.R;

/**
 * Created by koell on 17 Jan 2018.
 */

public class Dialogger {

    private Context context;
    private View dialogView;

    public Dialogger(Context context){
        this.context = context;

        dialogView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.my_prog_dialog, null);

    }

    public Dialogger setMessage(String msg){
        ((TextView) dialogView.findViewById(R.id.txtProg)).setText(msg);

        return this;
    }
    public Dialogger setMessage(@StringRes int resID){
        return setMessage(context.getString(resID));
    }

    public AlertDialog build(){
        return new AlertDialog.Builder(context)
                .setView(dialogView)
                .setCancelable(false)
                .create();
    }

}
