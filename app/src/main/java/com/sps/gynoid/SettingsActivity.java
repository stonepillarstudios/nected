package com.sps.gynoid;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import com.edmodo.rangebar.RangeBar;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.nispok.snackbar.Snackbar;
import com.sps.gynoid.utils.Dialogger;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;
import com.sps.gynoid.utils.SnappingSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SettingsActivity extends Activity {

    public static final int MINIMUM_AGE = 18;
    public static final int MAXIMUM_AGE = 80;
    private static final int RECOM_QUALITY = 60;

    private JanTools jantools;
    private PrefsHelper prefs;
    private boolean userStarted = false;
    private JSONArray settingsToUpdate;

    private RangeBar seek_age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (getActionBar() != null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        jantools = new JanTools(this);
        prefs = new PrefsHelper(this);
        settingsToUpdate = new JSONArray();

        ((SeekBar) findViewById(R.id.seek_quality)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((TextView) findViewById(R.id.txtMatchQuality)).setText(
                        String.format("%s: %d%%", getString(R.string.match_quality), progress)
                );
                if (progress <= RECOM_QUALITY){

                    float frac = (float) progress / RECOM_QUALITY;

                    setBarColorFrac(frac);

                }else{

                    float frac = (float) (RECOM_QUALITY-(progress-RECOM_QUALITY)) / RECOM_QUALITY;

                    setBarColorFrac(frac);

                }

            }
            private void setBarColorFrac(float frac){
                int srcColor = ((int) new ArgbEvaluator().evaluate(frac, Color.RED, Color.GREEN));

                SeekBar seekBar = findViewById(R.id.seek_quality);

                //set progress colour
                seekBar.getProgressDrawable().setColorFilter(
                        srcColor, PorterDuff.Mode.SRC_IN
                );

                //set thumb colour
                Drawable thumbDrawable = DrawableCompat.wrap(seekBar.getThumb());
                DrawableCompat.setTintList(thumbDrawable, ColorStateList.valueOf(srcColor));
                seekBar.setThumb(thumbDrawable);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (userStarted){
                    queueSettingForUpdate(RautenAPI.Setting.MATCH_QUALITY, seekBar.getProgress()+"");

                    if (seekBar.getProgress() <= 35 && !prefs.readBool(PrefsHelper.PREF_DIA_LOW_QUALITY)){
                        new AlertDialog.Builder(SettingsActivity.this)
                                .setMessage(R.string.dialog_low_quality)
                                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        prefs.write(PrefsHelper.PREF_DIA_LOW_QUALITY, true);
                                    }
                                })
                                .show();

                    }else if (seekBar.getProgress() > 90 && !prefs.readBool(PrefsHelper.PREF_DIA_HIGH_QUALITY)){
                        new AlertDialog.Builder(SettingsActivity.this)
                                .setMessage(R.string.dialog_high_quality)
                                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        prefs.write(PrefsHelper.PREF_DIA_HIGH_QUALITY, true);
                                    }
                                })
                                .show();
                    }

                }
            }
        });

        ((SnappingSeekBar) findViewById(R.id.seek_distance)).setOnItemSelectionListener(new SnappingSeekBar.OnItemSelectionListener() {
            @Override
            public void onItemSelected(int itemIndex, String itemString) {
                if (userStarted)
                    queueSettingForUpdate(RautenAPI.Setting.MAX_DISTANCE_INDEX, itemIndex+"");
            }
        });

        seek_age = findViewById(R.id.seek_age);
        seek_age.setTickHeight(0);
        seek_age.setTickCount(MAXIMUM_AGE-MINIMUM_AGE+1);
        seek_age.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP){

                    queueSettingForUpdate(RautenAPI.Setting.MIN_AGE, (seek_age.getLeftIndex()+MINIMUM_AGE) +"");
                    queueSettingForUpdate(RautenAPI.Setting.MAX_AGE, (seek_age.getRightIndex()+MINIMUM_AGE) +"");
                }

                return false;
            }
        });
        seek_age.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int thumbLeft, int thumbRight) {
                if (thumbLeft < 0){ //weird glitch
                    seek_age.setThumbIndices(0, seek_age.getRightIndex());
                }
                String leftStr = (seek_age.getLeftIndex()+MINIMUM_AGE) +"";
                ((TextView) findViewById(R.id.txtAgeStart)).setText(leftStr);
                String rightStr = (seek_age.getRightIndex()+MINIMUM_AGE) +"";
                ((TextView) findViewById(R.id.txtAgeEnd)).setText(
                        rightStr.equals(MAXIMUM_AGE + "") ? getString(R.string.age_max_100) : rightStr
                );
            }
        });
        seek_age.setConnectingLineColor(ContextCompat.getColor(SettingsActivity.this, R.color.colorAccent));
        seek_age.setThumbColorNormal(ContextCompat.getColor(SettingsActivity.this, R.color.colorAccentSemiTrans));
        seek_age.setThumbColorPressed(ContextCompat.getColor(SettingsActivity.this, R.color.colorAccent));


        new RautenAPI(RautenAPI.URL_SETTINGS_HANDLER, new Rjson()
                .put("action", RautenAPI.SettingAction.GET)
                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH)),
                new RautenAPI.Callback() {

            @Override
            public void onSuccess(@NonNull JSONObject result) throws JSONException{
                setSettings(result.getJSONArray("settings"));
                findViewById(R.id.laySettings).setVisibility(View.VISIBLE);
                findViewById(R.id.layLoader).setVisibility(View.GONE);
            }

            @Override
            public void onFail(@Nullable JSONObject result) {
                jantools.makeToast(R.string.settings_get_failed);
                SettingsActivity.this.setResult(RESULT_CANCELED);
                SettingsActivity.this.finish();
            }

        }).executeAsync();

    }

    private void setSettings(JSONArray settings){

        for (int i = 0; i < settings.length(); i++){
            try {
                JSONObject setting = ((JSONObject) settings.get(i));
                switch (setting.getString("setting_key")){

                    case RautenAPI.Setting.MATCH_QUALITY:
                        ((SeekBar) findViewById(R.id.seek_quality)).setProgress(
                                setting.getInt("setting_value")
                        );
                        ((TextView) findViewById(R.id.txtMatchQuality)).setText(
                                String.format("%s: %d%%", getString(R.string.match_quality), setting.getInt("setting_value"))
                        );
                        break;

                    case RautenAPI.Setting.MAX_DISTANCE_INDEX:
                        ((SnappingSeekBar) findViewById(R.id.seek_distance)).setProgressToIndex(
                                setting.getInt("setting_value")
                        );
                        break;

                    case RautenAPI.Setting.MIN_AGE:
                        seek_age.setThumbIndices(
                                setting.getInt("setting_value")-MINIMUM_AGE, seek_age.getRightIndex()
                        );
                        break;

                    case RautenAPI.Setting.MAX_AGE:
                        ((RangeBar) findViewById(R.id.seek_age)).setThumbIndices(
                                seek_age.getLeftIndex(), setting.getInt("setting_value")-MINIMUM_AGE
                        );
                        break;

                    case RautenAPI.Setting.LIKES_MEN:
                        if (setting.getBoolean("setting_value"))
                            ((CheckBox) findViewById(R.id.chkMale)).setChecked(true);
                        break;

                    case RautenAPI.Setting.LIKES_WOMEN:
                        if (setting.getBoolean("setting_value"))
                            ((CheckBox) findViewById(R.id.chkFemale)).setChecked(true);
                        break;

                }

            }catch (JSONException|ClassCastException e){
                e.printStackTrace();
            }
        }

        Log.i("devlog", "settings loaded & set");

    }

    private void queueSettingForUpdate(String key, String value){
        prefs.write(key, value);

        try{
            settingsToUpdate.put(new JSONObject()
                    .put("setting_key", key)
                    .put("setting_value", value)
            );
        }catch (JSONException e){
            e.printStackTrace();
            snack(getString(R.string.setting_not_saved), "ERROR");
        }
        snack(getString(R.string.setting_saved), "SUCCESS");
    }

    private void snack(String msg, String type){

        Snackbar snack = Snackbar.with(this)
                .text(msg)
                .duration(1000);

        switch (type) {
            case "ERROR":
                snack.colorResource(android.R.color.holo_red_dark)
                        .show(this);
                break;
            case "SUCCESS":
                snack.colorResource(android.R.color.holo_green_dark)
                        .show(this);
                break;
            default:
                Log.d("devlog", "unknown snack type: " + type);
                break;
        }

    }

    private void finishProperly(){

        if (settingsToUpdate.length() > 0){

            findViewById(R.id.layLoader).setVisibility(View.VISIBLE);
            findViewById(R.id.laySettings).setVisibility(View.GONE);

            new RautenAPI(RautenAPI.URL_SETTINGS_HANDLER, new Rjson()
                    .put("action", RautenAPI.SettingAction.SAVE)
                    .put("settings", settingsToUpdate)
                    .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH)),
                    new RautenAPI.Callback() {

                        @Override
                        public void onSuccess(@NonNull JSONObject result) {
                            Log.i("devlog", "settings uploaded");
                            setResult(RESULT_OK);
                            SettingsActivity.this.finish();
                        }

                        @Override
                        public void onFail(@Nullable JSONObject result) {
                            findViewById(R.id.layLoader).setVisibility(View.GONE);
                            findViewById(R.id.laySettings).setVisibility(View.VISIBLE);
                            snack(getString(R.string.settings_not_changed), "ERROR");
                        }

                    }
            ).executeAsync();

        }else{
            finish();
        }

    }

    @Override
    public void onUserInteraction() {
        userStarted = true;
    }

    public void onGenderClick(View v){

        if (userStarted) {

            CheckBox chk = ((CheckBox) v);
            CheckBox chkOther;

            if (chk.getId() == R.id.chkMale) {
                chkOther = findViewById(R.id.chkFemale);

                queueSettingForUpdate(RautenAPI.Setting.LIKES_MEN, chk.isChecked() + "");
            } else {
                chkOther = findViewById(R.id.chkMale);

                queueSettingForUpdate(RautenAPI.Setting.LIKES_WOMEN, chk.isChecked() + "");
            }

            if (!chk.isChecked() && !chkOther.isChecked()) {
                chkOther.setChecked(true);
                queueSettingForUpdate(
                        chkOther.getId() == R.id.chkMale ?
                                RautenAPI.Setting.LIKES_MEN :
                                RautenAPI.Setting.LIKES_WOMEN ,
                        chkOther.isChecked() + "");
            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                finishProperly();
                return true;

            case R.id.menu_done:
                finishProperly();
                return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        finishProperly();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    public void onLogoutClick(View view) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.logout_dia)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LoginManager.getInstance().logOut();
                        SettingsActivity.this.setResult(RESULT_OK);
                        SettingsActivity.this.finish();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void onDeregClick(View v){
        new AlertDialog.Builder(this)
                .setMessage(R.string.pre_deregistration_dia)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new RautenAPI(RautenAPI.URL_USER_HANDLER, new Rjson()
                                .put("action", "deregister")
                                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH)),
                                new RautenAPI.Callback() {

                                    @Override
                                    public void onSuccess(@NonNull JSONObject result) {
                                        prefs.write(PrefsHelper.PREF_REG_DONE, false);
                                        jantools.makeToast(R.string.dereg_done_toast);
                                        LoginManager.getInstance().logOut();
                                        SettingsActivity.this.setResult(RESULT_OK);
                                        SettingsActivity.this.finish();
                                    }

                                    @Override
                                    public void onFail(@Nullable JSONObject result) {
                                        jantools.makeToast(R.string.generic_fuckup);
                                    }
                                },
                                new Dialogger(SettingsActivity.this)
                                        .setMessage(R.string.dereg_loading_dia)
                                        .build()
                        ).executeAsync();

                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
