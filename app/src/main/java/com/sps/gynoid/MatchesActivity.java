package com.sps.gynoid;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.nispok.snackbar.Snackbar;
import com.sps.gynoid.frags.LoadingFragment;
import com.sps.gynoid.frags.MatchesFragment;
import com.sps.gynoid.frags.GenericMsgFrag;
import com.sps.gynoid.frags.StartInfoFrag;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.Match;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MatchesActivity extends FragmentActivity {

    private PrefsHelper prefs;
    private JanTools jantools;
    private static LocalBroadcastManager localBroadcastManager;
    private LocationManager locMan;
    private LocationListener locListener;
    private MenuItem mSettingsHook;
    private boolean hideSettings = false;
    private RetrievalLooper retrievalLooper = new RetrievalLooper();

    private static final int REQ_SETTINGS = 101;

    private static final String EVENT_MATCH_GET_FAIL = "EVENT_MATCH_GET_FAIL";
    public static final String EVENT_MATCHES_BEGOTTEN = "EVENT_MATCHES_BEGOTTEN";
    private static final String EVENT_STARTED_MATCHING = "EVENT_STARTED_MATCHING";
    private static final String EVENT_UPPING_LIKES = "EVENT_UPPING_LIKES";
    private static final String EVENT_REG_NOT_DONE = "EVENT_REGISTRATION_INCOMPLETE";
    private static final String EVENT_BAD_RAUTH = "EVENT_BAD_RAUTH";
    public static final String FILTER_LOCAL = "com.sps.gynoid.MatchesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        prefs = new PrefsHelper(this);
        prefs.setDefaults();
        jantools = new JanTools(this);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        if (savedInstanceState == null) {

            localBroadcastManager.registerReceiver(
                    compatsBrowserReceiver, new IntentFilter(FILTER_LOCAL)
            );

            locMan = ((LocationManager) getSystemService(LOCATION_SERVICE));
            locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location.getAccuracy() < 1_000) {
                        locMan.removeUpdates(this);
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
            try {
                locMan.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1, 1, locListener);
            } catch (SecurityException ignored) {
            } //will eventually cause "loc not found" error

            startDoingThings();

        }
    }

    //--------BroadcastReceivers
    private BroadcastReceiver compatsBrowserReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            switch (intent.getStringExtra("event")){

                case EVENT_MATCH_GET_FAIL:
                    GenericMsgFrag matchFailFrag = new GenericMsgFrag();
                    matchFailFrag.setMainText(getString(R.string.match_retrieval_failure));
                    matchFailFrag.showButton(getString(R.string.retry_btn), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startDoingThings();
                        }
                    });
                    updateFrag(matchFailFrag);
                    settingsVis(true);
                    break;

                case EVENT_MATCHES_BEGOTTEN:

                    Match[] matches = ((Match[]) intent.getParcelableArrayExtra("matches"));

                    settingsVis(true);

                    //save matches locally
                    prefs.write(PrefsHelper.PREF_LOCAL_MATCHES, matchesToJSONArr(matches).toString());

                    if (matches.length == 0){
                        //if it reaches here, there must literally be no other users in the db

                        GenericMsgFrag noMatchFragment = new GenericMsgFrag();
                        noMatchFragment.setMainText(getString(R.string.no_matches_waiting_txt));
                        noMatchFragment.setImgDrawable(ContextCompat.getDrawable(MatchesActivity.this, R.drawable.no_match_icon));
                        noMatchFragment.setCallback(new GenericMsgFrag.Callback() {
                            @Override
                            public void onRefresh() {
                                retrieveMatches();
                            }
                        });
                        updateFrag(noMatchFragment);

                    }else{

                        MatchesFragment matchesFrag = new MatchesFragment();
                        matchesFrag.setMatches(matches);
                        matchesFrag.setCallback(new MatchesFragment.MatchFragCallback() {
                            @Override
                            public void onSwipeRefresh() {
                                retrieveMatches();
                            }
                        });
                        updateFrag(matchesFrag);

                    }
                    break;

                case EVENT_STARTED_MATCHING:
                    LoadingFragment matchingFrag = new LoadingFragment();
                    String[] matchingTexts = getResources().getStringArray(R.array.matching_loading_texts);
                    matchingFrag.setLoadingText(JanTools.randArr(matchingTexts));
                    updateFrag(matchingFrag);
                    settingsVis(false);
                    break;

                case EVENT_UPPING_LIKES:
                    LoadingFragment uppingFrag = new LoadingFragment();
                    uppingFrag.setLoadingText(getString(R.string.sending_new_likes));

                    updateFrag(uppingFrag);
                    settingsVis(false);
                    break;

                case EVENT_BAD_RAUTH:
                    jantools.makeToast(R.string.account_creation_fail, true);
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(MatchesActivity.this, RegistrationActivity.class));
                    finish();
                    break;

                case EVENT_REG_NOT_DONE:
                    prefs.write(PrefsHelper.PREF_REG_DONE, false);
                    LoginManager.getInstance().logOut();
                    jantools.makeToast(R.string.finish_reg_toast);
                    startActivity(
                            new Intent(MatchesActivity.this, RegistrationActivity.class)
                    );
                    finish();
                    break;

            }

        }
    };

    private JSONArray matchesToJSONArr(Match[] matches) {

        JSONArray jarr = new JSONArray();
        for (Match match : matches){
            jarr.put(match.toJSON());
        }

        return jarr;

    }

    //sets settings menu item visible or invisible
    private void settingsVis(boolean show){
        hideSettings = !show;
        if (mSettingsHook != null)
            mSettingsHook.setVisible(show);
        //if hook is null, then menu hasn't been created yet.
        // The hideSettings bool schedules its hiding.
    }

    //this really gets things going. If called during app run, it will restart, so-to-speak
    private void startDoingThings(){
        Log.i("devlog", "======= Main Start ======");

        if (AccessToken.getCurrentAccessToken() == null || !prefs.readBool(PrefsHelper.PREF_REG_DONE)) {
            Log.i("devlog", "redirecting to login screen");
            LoginManager.getInstance().logOut();
            startActivity(new Intent(this, RegistrationActivity.class));
            finish();

        }else if (AccessToken.getCurrentAccessToken().getDeclinedPermissions().size() > 0){
            Log.w("devlog", "Some permissions declined. Redirecting to login screen");
            jantools.makeToast(R.string.need_perms_rationale);
            LoginManager.getInstance().logOut();
            startActivity(new Intent(this, RegistrationActivity.class));
            finish();

        }else{

            if(prefs.readBool(PrefsHelper.PREF_FIRST_RUN)){
                Log.i("devlog", "Showing first run info screen");
                settingsVis(false);
                updateFrag(StartInfoFrag.newInstance(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prefs.write(PrefsHelper.PREF_FIRST_RUN, false);
                        startDoingThings();
                    }
                }));

            }else if (Hours.hoursBetween(
                    new DateTime(prefs.readLong(PrefsHelper.PREF_LAST_LIKES_UPLOAD)),
                    new DateTime()).getHours() >= 24) {
                Log.i("devlog", "24hrs since last likes update. Updating likes & location");

                new GetAndUploadLocation().execute();
                updateLikes();
                prefs.write(PrefsHelper.PREF_LAST_LIKES_UPLOAD, new DateTime().getMillis());

            } else {
                showLocalMatches();

            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_compats_browser, menu);
        mSettingsHook = menu.findItem(R.id.menu_settings);
        if (hideSettings)
            mSettingsHook.setVisible(false);
        return jantools.checkConnection();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQ_SETTINGS && resultCode == RESULT_OK){
            startDoingThings();

        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        startDoingThings();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), REQ_SETTINGS);
                break;

            default:
                return false;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(compatsBrowserReceiver);
        if (locMan != null)
            locMan.removeUpdates(locListener);

        if (retrievalLooper.getStatus() == AsyncTask.Status.RUNNING){
            retrievalLooper.cancel(true);
        }
    }

    private void updateFrag(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainFrag, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commitAllowingStateLoss();
    }

    private Match[] jsonArrToMatches(JSONArray jmatches) throws JSONException{

        Match[] matches = new Match[jmatches.length()];
        for (int i = 0; i < jmatches.length(); i++){

            JSONObject jmatch = jmatches.getJSONObject(i);

            matches[i] = new Match(jmatch.getString("profileID"),
                    jmatch.getString("first_name"),
                    jmatch.getString("last_name"),
                    jmatch.getInt("age"),
                    jmatch.getDouble("quality"),
                    jmatch.getString("messengerUsername"),
                    jmatch.getInt("legitimacy") == 1
            );

        }

        return matches;

    }

    private void updateLikes(){

        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                .putExtra("event", EVENT_UPPING_LIKES)
        );

        new RautenAPI(RautenAPI.URL_USER_HANDLER, new Rjson()
                .put("accessToken", AccessToken.getCurrentAccessToken().getToken())
                .put("action", "update_likes"),
                new RautenAPI.Callback() {
                    @Override
                    public void onSuccess(@NonNull JSONObject result){
                        showLocalMatches();
                    }

                    @Override
                    public void onFail(@Nullable JSONObject result) {

                        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                                .putExtra("event", EVENT_MATCH_GET_FAIL)
                        );

                    }
                }

        ).executeAsync();


    }

    private void showLocalMatches(){

        try{

            JSONArray jmatches = new JSONArray(prefs.readStr(PrefsHelper.PREF_LOCAL_MATCHES));

            if (jmatches.length() == 0){ //no matches at all is a bad condition! Retrieve something!
                if (jantools.checkConnection()) {
                    retrieveMatches();
                }else{
                    localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                            .putExtra("event", EVENT_MATCHES_BEGOTTEN)
                            .putExtra("matches", new Match[0])
                    );
                }
            }else{

                localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                        .putExtra("event", EVENT_MATCHES_BEGOTTEN)
                        .putExtra("matches", jsonArrToMatches(jmatches))
                );

                Snackbar.with(this)
                        .text(getString(R.string.pull_to_refresh))
                        .colorResource(R.color.colorPrimary)
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                        .show(this);

            }

        }catch (JSONException e){
            Log.w("devlog", "failed to read local matches: " + e.getMessage());
            localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                    .putExtra("event", EVENT_MATCH_GET_FAIL)
            );
        }

    }

    private void retrieveMatches(){

        if (!jantools.checkConnection()){
            jantools.makeToast(R.string.offline_txt);
            showLocalMatches();
            return;
        }

        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                .putExtra("event", EVENT_STARTED_MATCHING)
        );

        deleteStoredPPs(); //so that pps may be refreshed

        new RautenAPI(RautenAPI.URL_MATCH_HANDLER, new Rjson()
                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH))
                .put("action", "refresh"),
                new RautenAPI.Callback() {
                    @Override
                    public void onSuccess(@NonNull JSONObject result) throws JSONException {
                        startRetrievalLooper();
                    }

                    @Override
                    public void onFail(@Nullable JSONObject result) {
                        if (result == null)
                            //assuming this is a timeout due to long matchmaking time
                            startRetrievalLooper();
                        else{

                            try{

                                switch (result.getString("failMsg")) {
                                    case "BAD_RAUTH":
                                        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                                                .putExtra("event", EVENT_BAD_RAUTH)
                                        );

                                        break;

                                    case "REGISTRATION_INCOMPLETE":
                                        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                                                .putExtra("event", EVENT_REG_NOT_DONE)
                                        );

                                        break;

                                    default:
                                        localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                                                .putExtra("event", EVENT_MATCH_GET_FAIL)
                                        );

                                        break;
                                }

                            }catch (JSONException e){
                                localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                                        .putExtra("event", EVENT_MATCH_GET_FAIL)
                                );
                            }

                        }
                    }
                }
        ).executeAsync();

    }

    private void deleteStoredPPs(){

        File ppPath = new File(getFilesDir().getAbsolutePath() + "/" + MatchesFragment.DIR_LOCAL_PP);

        if (ppPath.exists()){

            for (File ppFile : ppPath.listFiles()){
                ppFile.delete();
            }

        }

    }

    private void startRetrievalLooper(){

        if (retrievalLooper.getStatus() == AsyncTask.Status.PENDING)
            retrievalLooper.execute();
        else if (retrievalLooper.getStatus() == AsyncTask.Status.FINISHED){
            retrievalLooper = new RetrievalLooper();
            retrievalLooper.execute();
        }//else if RUNNING, do nothing

    }

    private class RetrievalLooper extends AsyncTask<Void, Void, Boolean>{

        private boolean retrieveSuccess = false;
        private int loops = 0;
        private Match[] matches;
        private static final int MAX_LOOPS = 8;
        private static final int WAIT_PERIOD_MS = 6000;

        @Override
        protected Boolean doInBackground(Void... voids) {

            while (!retrieveSuccess && loops < MAX_LOOPS){

                if (isCancelled())
                    return false;

                try{

                    JSONObject result = new RautenAPI(RautenAPI.URL_MATCH_HANDLER)
                            .makeReq(new Rjson()
                                    .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH))
                                    .put("action", "retrieve")
                            );

                    if (result.getBoolean("success")){
                        JSONArray jmatches = result.getJSONArray("matches");

                        Log.i("devlog", jmatches.length()+" match" +
                                (jmatches.length() != 1 ? "es" : "") + " retrieved"
                        );

                        matches = jsonArrToMatches(jmatches);
                        retrieveSuccess = true;

                    }else if (!result.getString("failMsg").equals("MATCHING_BUSY")){
                        Log.w("devlog", "got something weird from rautenAPI: "+ result.toString()+"\nignoring it...");
                    }

                }catch(IOException|JSONException e){
                    Log.w("devlog", "there was a big problem retrieving matches:"+e.getMessage()+"\nignoring...");
                }finally {
                    loops++;
                }

                if (!retrieveSuccess){
                    try{
                        Thread.sleep(WAIT_PERIOD_MS);
                    }catch (InterruptedException e){
                        Log.w("devlog", "retrieval looper interrupted.");
                        return false;
                    }
                }

            }

            return retrieveSuccess;

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success && !isCancelled()){

                localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                        .putExtra("event", EVENT_MATCHES_BEGOTTEN)
                        .putExtra("matches", matches)
                );

            }else{

                localBroadcastManager.sendBroadcast(new Intent(FILTER_LOCAL)
                        .putExtra("event", EVENT_MATCH_GET_FAIL)
                );

            }

        }
    }

    //entirely in the background. Failing is fine.
    private class GetAndUploadLocation extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {

            try{

                if (locMan == null){
                    Log.w("devlog", "locMan is null!");
                    return false;
                }

                List<String> providers = locMan.getProviders(true);
                Location bestLocation = null;
                for (String provider: providers){
                    Location loc = locMan.getLastKnownLocation(provider);
                    if (loc==null)
                        continue;
                    if (bestLocation==null || loc.getAccuracy() < bestLocation.getAccuracy())
                        bestLocation = loc;
                }

                if (bestLocation == null)
                    return false;

                JSONObject received = new RautenAPI(RautenAPI.URL_LOCATION_HANDLER).makeReq(
                        new Rjson()
                                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH))
                                .put("coords", new JSONObject()
                                        .put("x", bestLocation.getLongitude())
                                        .put("y", bestLocation.getLatitude())
                                )
                );

                return received.getBoolean("success");

            }catch (JSONException| IOException| NullPointerException| SecurityException e){
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result){
                Log.i("devlog", "Location uploaded");
            }else {
                Log.w("devlog", "Location upload failed");
            }
        }
    }
}
