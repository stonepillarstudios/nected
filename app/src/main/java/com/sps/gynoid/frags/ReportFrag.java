package com.sps.gynoid.frags;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.sps.gynoid.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by koell on 29 Mar 2018.
 */

public class ReportFrag extends Fragment {

    private static final String REASON_SPAM = "SPAM";
    private static final String REASON_OFFENSIVE = "OFFENSIVE";
    private static final String REASON_USERNAME_WRONG = "USERNAME_WRONG";

    private List<String> reasons;
    private Callback callback;

    private ViewGroup viewGroup = null;

    public ReportFrag(){
        reasons = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        viewGroup = ((ViewGroup) inflater.inflate(R.layout.dialog_report, container, false));

        viewGroup.findViewById(R.id.chkReasonSpam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleReason(((CheckBox) v).isChecked(), REASON_SPAM);
            }
        });
        viewGroup.findViewById(R.id.chkReasonOffensive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleReason(((CheckBox) v).isChecked(), REASON_OFFENSIVE);
            }
        });
        viewGroup.findViewById(R.id.chkReasonWrongUsername).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleReason(((CheckBox) v).isChecked(), REASON_USERNAME_WRONG);
            }
        });
        viewGroup.findViewById(R.id.chkReasonOther).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()){
                    viewGroup.findViewById(R.id.edtReasonOther).setVisibility(View.VISIBLE);
                }else{
                    viewGroup.findViewById(R.id.edtReasonOther).setVisibility(View.GONE);
                }
                updateSubmitButton();
            }
        });

        viewGroup.findViewById(R.id.btnReportDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otherText = ((EditText) viewGroup.findViewById(R.id.edtReasonOther)).getText().toString();

                toggleReason(((CheckBox) viewGroup.findViewById(R.id.chkReasonOther)).isChecked(),
                        otherText.equals("") ? "OTHER" : otherText
                );
                callback.reportDone(reasons.toArray(new String[reasons.size()]));
            }
        });

        return viewGroup;

    }

    private void toggleReason(boolean shouldExist, String reason){
        if (shouldExist && !reasons.contains(reason))
            reasons.add(reason);
        else
            reasons.remove(reason); //does nothing if already not present

        updateSubmitButton();
    }

    private void updateSubmitButton(){
        viewGroup.findViewById(R.id.btnReportDone).setEnabled(
                reasons.size() > 0 ||
                        ((CheckBox) viewGroup.findViewById(R.id.chkReasonOther)).isChecked()
        );
    }

    private void setCallback(Callback callback) {
        this.callback = callback;
    }

    public static ReportFrag newInstance(Callback callback){

        ReportFrag frag = new ReportFrag();
        frag.setCallback(callback);

        return frag;
    }

    public interface Callback{
        void reportDone(String[] reasons);
    }

}
