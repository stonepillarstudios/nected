package com.sps.gynoid.frags;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sps.gynoid.R;

public class GenericMsgFrag extends Fragment {


    public GenericMsgFrag() {
        // Required empty public constructor
    }

    private String mainText = "";
    private String btnText = "";
    private Drawable imgDrawable = null;
    private View.OnClickListener onRetryListener = null;
    private Callback callback = null;

    public void setMainText(String str){
        mainText = str;
    }
    public void setImgDrawable(Drawable drawable){
        this.imgDrawable = drawable;
    }
    public void showButton(String btnText, View.OnClickListener onClickListener){
        this.btnText = btnText;
        this.onRetryListener = onClickListener;
    }
    public void setCallback(Callback callback){
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_generic_message, container, false);
        ((TextView) view.findViewById(R.id.txtNoMatches)).setText(mainText);
        if (!btnText.equals("") && onRetryListener != null) {
            Button btnRetry = view.findViewById(R.id.btnRetryNoMatches);
            btnRetry.setText(btnText);
            btnRetry.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(onRetryListener);
        }
        if (imgDrawable != null){
            ImageView imgView = view.findViewById(R.id.imgGeneric);
            imgView.setImageDrawable(imgDrawable);
            imgView.setVisibility(View.VISIBLE);
        }

        ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshGeneric)).setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (callback != null)
                            callback.onRefresh();
                    }
                }
        );

        return view;
    }

    public interface Callback{
        void onRefresh();
    }

}
