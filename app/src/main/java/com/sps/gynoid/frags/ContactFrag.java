package com.sps.gynoid.frags;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;
import com.sps.gynoid.R;
import com.sps.gynoid.utils.Dialogger;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by koell on 29 Mar 2018.
 */

public class ContactFrag extends Fragment {

    private Context context;
    private JanTools jantools;
    private ContactDoneListener contactDoneListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_contact, container, false);

        context = inflater.getContext();
        jantools = new JanTools(context);

        if (jantools.isPackageInstalled("com.facebook.orca")){
            ((ImageView) v.findViewById(R.id.imgMessenger)).setImageDrawable(
                    jantools.getAppIcon("com.facebook.orca")
            );
        }

        v.findViewById(R.id.wheredoe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(context)
                        .setView(inflater.inflate(R.layout.dialog_where_messenger_username, null, false))
                        .setNeutralButton(R.string.open_messenger, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (jantools.isPackageInstalled("com.facebook.orca")){
                                    jantools.openExternalApp("com.facebook.orca");
                                }else{
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.me")));
                                }
                            }
                        })
                        .setPositiveButton(android.R.string.ok, null)
                        .show();

            }
        });

        v.findViewById(R.id.whydoe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setMessage(R.string.messenger_username_required_rationale)
                        .setNeutralButton(android.R.string.ok, null)
                        .show();
            }
        });

        v.findViewById(R.id.btnContactNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = ((EditText) v.findViewById(R.id.edtMessengerUsername)).getText().toString().trim();
                if (username.contains("m.me/"))
                    username = username.substring(username.indexOf("m.me/")+"m.me/".length());

                if (acceptableChars(username)){

                    new RautenAPI(RautenAPI.URL_USER_HANDLER, new Rjson()
                            .put("profileID", Profile.getCurrentProfile().getId())
                            .put("action", "set_messenger")
                            .put("messengerUsername", username)
                            .put("rauth", new PrefsHelper(context).readStr(PrefsHelper.PREF_RAUTH)),

                            new RautenAPI.Callback() {

                                @Override
                                public void onSuccess(@NonNull JSONObject result) {
                                    contactDoneListener.onContactDetailsVerified();
                                }

                                @Override
                                public void onFail(@Nullable JSONObject result) {
                                    if (result == null)
                                        fail();
                                    else{

                                        try {

                                            if (result.getString("failMsg").equals("BAD_USERNAME")) {
                                                jantools.makeToast(R.string.username_invalid_toast);

                                            } else if (result.getString("failMsg").equals("USERNAME_ALREADY_REGISTERED")) {
                                                new AlertDialog.Builder(context)
                                                        .setMessage(R.string.username_already_registered_dia)
                                                        .setPositiveButton(R.string.send_email_dia_btn, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                Intent intent = new Intent(Intent.ACTION_SENDTO);
                                                                intent.setType("message/rfc822");
                                                                intent.setData(Uri.parse("mailto:" + getString(R.string.dev_email)));
                                                                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.username_reset_req_email_subject));

                                                                startActivity(Intent.createChooser(intent, getString(R.string.send_email_dia_btn)));
                                                            }
                                                        })
                                                        .setNegativeButton(R.string.nevermind_dia_btn, null)
                                                        .show();
                                            } else {
                                                Log.w("devlog", "the rauten server had this to say: " + result.getString("failMsg"));
                                                fail();
                                            }

                                        }catch (JSONException e){
                                            e.printStackTrace(); fail();
                                        }

                                    }
                                }

                                private void fail(){
                                    jantools.makeToast(R.string.username_check_failed);
                                }
                            },

                            new Dialogger(context)
                                    .setMessage(R.string.checking_messenger_username)
                                    .build()

                    ).executeAsync();

                }else{
                    jantools.makeToast(R.string.username_invalid);
                }
            }

            private boolean acceptableChars(String subject){
                if (subject.length() == 0)
                    return false;
                for (int i = 0; i < subject.length(); i++){
                    if ( !Character.isLetterOrDigit(subject.charAt(i)) && subject.charAt(i) != '.'
                            && subject.charAt(i) != '_' && subject.charAt(i) != '-')
                        return false;
                }
                return true;
            }
        });

        return v;

    }

    protected void setOnContactDoneListener(ContactDoneListener listener){
        this.contactDoneListener = listener;
    }

    public static ContactFrag newInstance(ContactDoneListener listener){
        ContactFrag frag = new ContactFrag();
        frag.setOnContactDoneListener(listener);

        return frag;
    }

    public interface ContactDoneListener{
        void onContactDetailsVerified();
    }
}
