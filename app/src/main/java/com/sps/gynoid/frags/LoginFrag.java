package com.sps.gynoid.frags;

import android.Manifest;
import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.sps.gynoid.R;
import com.sps.gynoid.utils.Dialogger;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

public class LoginFrag extends Fragment {

    private final String[] perms = new String[]{
            "email",
            "public_profile",
            "user_likes",
            "user_birthday",
            "user_gender"
    };

    private Context context;
    private JanTools jantools;
    private PrefsHelper prefs;

    private LocationManager locMan;
    private LocationListener locListener = null;

    private CallbackManager callbackManager;
    private OnLoginDoneListener loginDoneListener = null;
    private char forceGender = 'X';
    private boolean alreadyRegistered = false;
    private boolean prelimRegDone = false;
    private AlertDialog registrationDia;

    private static final int REQ_PERMS = 301;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        context = inflater.getContext();
        jantools = new JanTools(context);
        prefs = new PrefsHelper(context);
        locMan = ((LocationManager) context.getSystemService(LOCATION_SERVICE));

        if (locListener == null) {
            locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location.getAccuracy() < 1_000) {
                        locMan.removeUpdates(this);
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
            try {
                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locListener);
            } catch (SecurityException ignored) {
            } //will eventually cause "loc not found" error
        }

        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = v.findViewById(R.id.fb_login_button);

        loginButton.setReadPermissions(perms);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getAccessToken() != null){

                    if (loginResult.getAccessToken().getDeclinedPermissions().size() > 0){
                        jantools.makeToast(R.string.need_perms_rationale, true);
                        LoginManager.getInstance().logInWithReadPermissions(
                                LoginFrag.this, Arrays.asList(perms)
                        );
                    }else {

                        if(Profile.getCurrentProfile() == null) {
                            registrationDia.show();
                            new ProfileTracker() {
                                @Override
                                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                                    if (currentProfile != null) {
                                        this.stopTracking();
                                        doPrelimReg();
                                    }
                                }
                            };

                        } else {
                            doPrelimReg();
                        }

                    }

                }else{
                    jantools.makeToast(R.string.fb_login_issue, true);
                }
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
                jantools.makeToast(R.string.fb_login_issue, true);
            }
        });

        registrationDia = new Dialogger(context)
                .setMessage(R.string.registering_loading_text)
                .build();

        return v;
    }

    private void setOnLoginDoneListener(OnLoginDoneListener listener){
        this.loginDoneListener = listener;
    }

    public static LoginFrag newInstance(OnLoginDoneListener listener){

        LoginFrag frag = new LoginFrag();
        frag.setOnLoginDoneListener(listener);

        return frag;

    }

    private void doPrelimReg(){

        try {

            if (locMan == null){
                Log.w("devlog", "locMan is null!");
                locationFail();
                return;
            }

            List<String> providers = locMan.getProviders(true);
            Location bestLocation = null;
            for (String provider: providers){
                Location loc = locMan.getLastKnownLocation(provider);
                if (loc==null)
                    continue;
                if (bestLocation==null || loc.getAccuracy() < bestLocation.getAccuracy())
                    bestLocation = loc;
            }

            if (bestLocation == null) {
                locationFail();
                return;
            }


            Rjson jsonToSend = new Rjson()
                    .put("action", "register|update_location")
                    .put("accessToken", AccessToken.getCurrentAccessToken().getToken())
                    .put("coords", new Rjson()
                        .put("x", bestLocation.getLongitude())
                        .put("y", bestLocation.getLatitude())
            );
            if (forceGender != 'X') {
                jsonToSend.put("forceGender", forceGender);
            }

            new RautenAPI(RautenAPI.URL_USER_HANDLER, jsonToSend,
                    new RautenAPI.Callback() {

                        @Override
                        public void onSuccess(@NonNull JSONObject result) throws JSONException{
                            prefs.write(PrefsHelper.PREF_RAUTH, result.getString("rauth"));
                            succeed(result.getBoolean("user_registered"));
                        }

                        @Override
                        public void onFail(@Nullable JSONObject result) {

                            if (result == null)
                                fail("Unknown");
                            else{

                                try{
                                    JSONArray issues_arr = result.getJSONObject("user_readiness").getJSONArray("issues_array");

                                    if (issues_arr.length() == 0) {
                                        fail("No issues returned!");
                                    }else {
                                        List<String> issues = new ArrayList<>(issues_arr.length());
                                        for (int i = 0; i < issues_arr.length(); i++) {
                                            issues.add(issues_arr.getString(i));
                                        }

                                        if (issues.contains("DOB_NOT_SET")){
                                            LoginManager.getInstance().logOut();
                                            new AlertDialog.Builder(context)
                                                    .setMessage(R.string.birthday_required_rationale)
                                                    .setNeutralButton(android.R.string.ok, null)
                                                    .show();

                                        }else if (issues.contains("GENDER_NOT_SET")){
                                            new AlertDialog.Builder(context)
                                                    .setMessage(R.string.which_gender)
                                                    .setPositiveButton(getString(R.string.btn_male), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {

                                                            forceGender = 'M';
                                                            doPrelimReg();

                                                        }
                                                    }).setNegativeButton(getString(R.string.btn_female), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    forceGender = 'F';
                                                    doPrelimReg();

                                                }
                                            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                                                @Override
                                                public void onCancel(DialogInterface dialogInterface) {
                                                    LoginManager.getInstance().logOut();
                                                }
                                            }).show();

                                        }else if (issues.contains("TOO_FEW_LIKES")){
                                            LoginManager.getInstance().logOut();
                                            new AlertDialog.Builder(context)
                                                    .setMessage(R.string.too_few_pages_rationale)
                                                    .setNeutralButton(android.R.string.ok, null)
                                                    .show();


                                        }else{ //messenger_username_not_set or nothing
                                            prefs.write(PrefsHelper.PREF_RAUTH, result.getString("rauth"));
                                            succeed(result.getBoolean("user_registered"));
                                        }

                                    }

                                }catch (JSONException e){
                                    fail("bad json: " + result.toString());
                                }

                            }

                        }

                        private void succeed(boolean alreadyRegistered){
                            Log.i("devlog", "preliminary registration complete");
                            prelimRegDone = true;
                            LoginFrag.this.alreadyRegistered = alreadyRegistered;
                            prefs.write(PrefsHelper.PREF_LAST_LIKES_UPLOAD, new Date().getTime());
                            checkDevicePerms(); //next step
                        }
                        private void fail(String msg){
                            Log.w("devlog", "preliminary registration failed: " + msg);
                            LoginManager.getInstance().logOut();
                            jantools.makeToast(R.string.reg_fail, true);
                        }
                    },
                    registrationDia
            ).executeAsync();
        }catch (SecurityException sece){
            locationFail();
        }

    }

    private void locationFail(){locationFail(false);}
    private void locationFail(boolean explicitlyDenied){

        if (explicitlyDenied){
            LoginManager.getInstance().logOut();
            new AlertDialog.Builder(context)
                    .setMessage(R.string.no_location_msg)
                    .setNeutralButton(android.R.string.ok, null)
                    .show();

        }else if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            new AlertDialog.Builder(context)
                    .setMessage(R.string.location_necessary)
                    .setCancelable(false)
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQ_PERMS);
                        }
                    })
                    .show();

        }else { //something else went wrong
            LoginManager.getInstance().logOut();
            new AlertDialog.Builder(context)
                    .setMessage(R.string.no_location_msg)
                    .setNeutralButton(android.R.string.ok, null)
                    .show();
        }
    }

    private void checkDevicePerms(){checkDevicePerms(false);}
    private void checkDevicePerms(boolean explicitlyDenied){

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if (prelimRegDone)
                gotoNextActivity();
            else
                doPrelimReg();
        }else{
            locationFail(explicitlyDenied);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQ_PERMS){
            checkDevicePerms(grantResults[0] == PackageManager.PERMISSION_DENIED);
        }
    }

    private void gotoNextActivity(){
        prefs.write(PrefsHelper.PREF_FIRST_RUN, true);

        if (loginDoneListener == null)
            Log.w("devlog", "loginDoneListener not set!");
        else
            loginDoneListener.onLoginDone(alreadyRegistered);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public interface OnLoginDoneListener{
        void onLoginDone(boolean alreadyRegistered);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locMan != null && locListener != null)
            locMan.removeUpdates(locListener);
    }
}
