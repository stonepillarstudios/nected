package com.sps.gynoid.frags;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.edmodo.rangebar.RangeBar;
import com.facebook.Profile;
import com.nispok.snackbar.Snackbar;
import com.sps.gynoid.R;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;
import com.sps.gynoid.utils.SnappingSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by koell on 27 Mar 2018.
 */

public class SetsSetupFrag extends Fragment {

    public static final int MINIMUM_AGE = 18;
    public static final int MAXIMUM_AGE = 80;

    private Context context;
    private PrefsHelper prefs;
    private JanTools jantools;
    private JSONArray settingsToUpdate;
    private OnSetsSetUpListener setupDoneListener = null;

    private boolean programSettingDone = false;

    private RangeBar seek_age;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_settings, container, false);

        context = inflater.getContext();
        prefs = new PrefsHelper(context);
        jantools = new JanTools(context);
        settingsToUpdate = new JSONArray();

        v.findViewById(R.id.seek_quality).setVisibility(View.GONE);
        v.findViewById(R.id.txtMatchQuality).setVisibility(View.GONE);

        ((SnappingSeekBar) v.findViewById(R.id.seek_distance)).setOnItemSelectionListener(new SnappingSeekBar.OnItemSelectionListener() {
            @Override
            public void onItemSelected(int itemIndex, String itemString) {

                queueSettingForUpdate(RautenAPI.Setting.MAX_DISTANCE_INDEX, itemIndex+"");
            }
        });

        seek_age = v.findViewById(R.id.seek_age);
        seek_age.setTickHeight(0);
        seek_age.setTickCount(MAXIMUM_AGE-MINIMUM_AGE+1);
        seek_age.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP){

                    queueSettingForUpdate(RautenAPI.Setting.MIN_AGE, (seek_age.getLeftIndex()+MINIMUM_AGE) +"");
                    queueSettingForUpdate(RautenAPI.Setting.MAX_AGE, (seek_age.getRightIndex()+MINIMUM_AGE) +"");
                }

                return false;
            }
        });
        seek_age.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int thumbLeft, int thumbRight) {
                if (thumbLeft < 0){ //weird glitch
                    seek_age.setThumbIndices(0, seek_age.getRightIndex());
                }
                String leftStr = (seek_age.getLeftIndex()+MINIMUM_AGE) +"";
                ((TextView) v.findViewById(R.id.txtAgeStart)).setText(leftStr);
                String rightStr = (seek_age.getRightIndex()+MINIMUM_AGE) +"";
                ((TextView) v.findViewById(R.id.txtAgeEnd)).setText(
                        rightStr.equals(MAXIMUM_AGE + "") ? getString(R.string.age_max_100) : rightStr
                );
            }
        });
        seek_age.setConnectingLineColor(ContextCompat.getColor(inflater.getContext(), R.color.colorAccent));
        seek_age.setThumbColorNormal(ContextCompat.getColor(inflater.getContext(), R.color.colorAccentSemiTrans));
        seek_age.setThumbColorPressed(ContextCompat.getColor(inflater.getContext(), R.color.colorAccent));

        View.OnClickListener onGenderClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckBox chk = ((CheckBox) view);
                CheckBox chkOther;

                if (chk.getId() == R.id.chkMale) {
                    chkOther = v.findViewById(R.id.chkFemale);

                    queueSettingForUpdate(RautenAPI.Setting.LIKES_MEN, chk.isChecked() + "");
                } else {
                    chkOther = v.findViewById(R.id.chkMale);

                    queueSettingForUpdate(RautenAPI.Setting.LIKES_WOMEN, chk.isChecked() + "");
                }

                if (!chk.isChecked() && !chkOther.isChecked()) {
                    chkOther.setChecked(true);
                    queueSettingForUpdate(
                            chkOther.getId() == R.id.chkMale ?
                                    RautenAPI.Setting.LIKES_MEN :
                                    RautenAPI.Setting.LIKES_WOMEN ,
                            chkOther.isChecked() + "");
                }

            }
        };

        v.findViewById(R.id.chkMale).setOnClickListener(onGenderClick);
        v.findViewById(R.id.chkFemale).setOnClickListener(onGenderClick);

        v.findViewById(R.id.btnLogout).setVisibility(View.GONE);
        v.findViewById(R.id.btnDeregister).setVisibility(View.GONE);
        v.findViewById(R.id.txtRegistrationInfo).setVisibility(View.VISIBLE);
        v.findViewById(R.id.btnSetupNext).setVisibility(View.VISIBLE);
        v.findViewById(R.id.btnSetupNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishProperly();
            }
        });

        return v;
    }

    private void retrieveSettings(){

        final Activity ctx = (Activity) context;

        new RautenAPI(RautenAPI.URL_SETTINGS_HANDLER, new Rjson()
                .put("profileID", Profile.getCurrentProfile().getId())
                .put("action", RautenAPI.SettingAction.GET)
                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH)),
                new RautenAPI.Callback() {

                    @Override
                    public void onSuccess(@NonNull JSONObject result) throws JSONException{
                        Log.i("devlog", "Settings retrieved");
                        setSettings(result.getJSONArray("settings"));
                        ctx.findViewById(R.id.laySettings).setVisibility(View.VISIBLE);
                        ctx.findViewById(R.id.layLoader).setVisibility(View.GONE);
                    }

                    @Override
                    public void onFail(@Nullable JSONObject result) {
                        jantools.makeToast(getString(R.string.default_settings_get_fail));
                        programSettingDone = false; // reload on next visible
                        setupDoneListener.setsFail();
                    }

                }).executeAsync();

        programSettingDone = true;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!programSettingDone && isVisibleToUser){

            if (Profile.getCurrentProfile() == null){ //fucking facebook
                jantools.makeToast(R.string.settings_get_failed);
                setupDoneListener.setsFail();

            }else {
                retrieveSettings();
            }

        }
    }

    private void setSettings(JSONArray settings){

        Activity ctx = (Activity) context;

        for (int i = 0; i < settings.length(); i++){
            try {
                JSONObject setting = ((JSONObject) settings.get(i));
                switch (setting.getString("setting_key")){

                    case RautenAPI.Setting.MAX_DISTANCE_INDEX:
                        ((SnappingSeekBar) ctx.findViewById(R.id.seek_distance)).setProgressToIndex(
                                setting.getInt("setting_value")
                        );
                        break;

                    case RautenAPI.Setting.MIN_AGE:
                        seek_age.setThumbIndices(
                                setting.getInt("setting_value")-MINIMUM_AGE, seek_age.getRightIndex()
                        );
                        break;

                    case RautenAPI.Setting.MAX_AGE:
                        ((RangeBar) ctx.findViewById(R.id.seek_age)).setThumbIndices(
                                seek_age.getLeftIndex(), setting.getInt("setting_value")-MINIMUM_AGE
                        );
                        break;

                    case RautenAPI.Setting.LIKES_MEN:
                        if (setting.getBoolean("setting_value"))
                            ((CheckBox) ctx.findViewById(R.id.chkMale)).setChecked(true);
                        break;

                    case RautenAPI.Setting.LIKES_WOMEN:
                        if (setting.getBoolean("setting_value"))
                            ((CheckBox) ctx.findViewById(R.id.chkFemale)).setChecked(true);
                        break;

                }

            }catch (JSONException|ClassCastException e){
                e.printStackTrace();
            }
        }

    }

    private void queueSettingForUpdate(String key, String value){
        prefs.write(key, value);

        try{
            settingsToUpdate.put(new JSONObject()
                    .put("setting_key", key)
                    .put("setting_value", value)
            );
        }catch (JSONException e){
            e.printStackTrace();
            snack(getString(R.string.setting_not_saved), "ERROR");
        }
        snack(getString(R.string.setting_saved), "SUCCESS");
    }

    private void snack(String msg, String type){

        if (programSettingDone) {

            Snackbar snack = Snackbar.with(context)
                    .text(msg);

            switch (type) {
                case "ERROR":
                    snack.colorResource(android.R.color.holo_red_dark)
                            .duration(1500)
                            .show((Activity) context);
                    break;
                case "SUCCESS":
                    snack.colorResource(android.R.color.holo_green_dark)
                            .duration(800)
                            .show((Activity) context);
                    break;
                default:
                    Log.d("devlog", "unknown snack type: " + type);
                    break;
            }
        }

    }

    private void finishProperly(){

        ((Activity) context).findViewById(R.id.layLoader).setVisibility(View.VISIBLE);
        ((Activity) context).findViewById(R.id.laySettings).setVisibility(View.GONE);


        new RautenAPI(RautenAPI.URL_SETTINGS_HANDLER, new Rjson()
                .put("action", RautenAPI.SettingAction.SAVE)
                .put("settings", settingsToUpdate)
                .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH)),
                new RautenAPI.Callback() {

                    @Override
                    public void onSuccess(@NonNull JSONObject result) {
                        Log.i("devlog", "initial settings set up");
                        setupDoneListener.setsSetUp();
                    }

                    @Override
                    public void onFail(@Nullable JSONObject result) {
                        Log.w("devlog", "could not set up initial settings");
                        ((Activity)context).findViewById(R.id.layLoader).setVisibility(View.GONE);
                        ((Activity)context).findViewById(R.id.laySettings).setVisibility(View.VISIBLE);
                        snack(getString(R.string.settings_not_changed), "ERROR");
                    }

                }
        ).executeAsync();

    }

    public void setSetsSetUpListener(OnSetsSetUpListener listener){
        this.setupDoneListener = listener;
    }

    public static SetsSetupFrag newInstance(OnSetsSetUpListener listener){
        SetsSetupFrag frag = new SetsSetupFrag();
        frag.setSetsSetUpListener(listener);
        return frag;
    }

    public interface OnSetsSetUpListener {
        void setsSetUp();
        void setsFail();
    }

}
