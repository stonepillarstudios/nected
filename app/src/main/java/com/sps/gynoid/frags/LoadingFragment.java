package com.sps.gynoid.frags;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sps.gynoid.R;


public class LoadingFragment extends Fragment {

    public LoadingFragment() {
        // Required empty public constructor
    }

    private String loadingText = "Loading";

    public void setLoadingText(String str){
        loadingText = str;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.frag_loading, container, false);
        ((TextView) view.findViewById(R.id.txtProg)).setText(loadingText);

        return view;
    }
}
